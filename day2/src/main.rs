fn main() {
    let spreadsheet = vec![
        vec![1136, 1129, 184, 452, 788, 1215, 355, 1109, 224, 1358, 1278, 176, 1302, 186, 128, 1148],
        vec![242, 53, 252, 62, 40, 55, 265, 283, 38, 157, 259, 226, 322, 48, 324, 299],
        vec![2330, 448, 268, 2703, 1695, 2010, 3930, 3923, 179, 3607, 217, 3632, 1252, 231, 286, 3689],
        vec![89, 92, 903, 156, 924, 364, 80, 992, 599, 998, 751, 827, 110, 969, 979, 734],
        vec![100, 304, 797, 81, 249, 1050, 90, 127, 675, 1038, 154, 715, 79, 1116, 723, 990],
        vec![1377, 353, 3635, 99, 118, 1030, 3186, 3385, 1921, 2821, 492, 3082, 2295, 139, 125, 2819],
        vec![3102, 213, 2462, 116, 701, 2985, 265, 165, 248, 680, 3147, 1362, 1026, 1447, 106, 2769],
        vec![5294, 295, 6266, 3966, 2549, 701, 2581, 6418, 5617, 292, 5835, 209, 2109, 3211, 241, 5753],
        vec![158, 955, 995, 51, 89, 875, 38, 793, 969, 63, 440, 202, 245, 58, 965, 74],
        vec![62, 47, 1268, 553, 45, 60, 650, 1247, 1140, 776, 1286, 200, 604, 399, 42, 572],
        vec![267, 395, 171, 261, 79, 66, 428, 371, 257, 284, 65, 25, 374, 70, 389, 51],
        vec![3162, 3236, 1598, 4680, 2258, 563, 1389, 3313, 501, 230, 195, 4107, 224, 225, 4242, 4581],
        vec![807, 918, 51, 1055, 732, 518, 826, 806, 58, 394, 632, 36, 53, 119, 667, 60],
        vec![839, 253, 1680, 108, 349, 1603, 1724, 172, 140, 167, 181, 38, 1758, 1577, 748, 1011],
        vec![1165, 1251, 702, 282, 1178, 834, 211, 1298, 382, 1339, 67, 914, 1273, 76, 81, 71],
        vec![6151, 5857, 4865, 437, 6210, 237, 37, 410, 544, 214, 233, 6532, 2114, 207, 5643, 6852]
    ];

    println!("Checksum part 1: {}", calculate_checksum(spreadsheet.clone())); // 37923
    println!("Checksum part 2: {}", calculate_checksum_part2(spreadsheet.clone())); // 263
}

fn calculate_checksum(spreadsheet: Vec<Vec<i32>>) -> i32 {
    let mut sum = 0;

    for row in spreadsheet {
        sum += calculate_difference_biggest_smallest(row);
    }

    sum
}

fn calculate_checksum_part2(spreadsheet: Vec<Vec<i32>>) -> i32 {
    let mut sum = 0;

    for row in spreadsheet {
        sum += calculate_difference_divides(row);
    }

    sum
}

fn calculate_difference_biggest_smallest(vec: Vec<i32>) -> i32 {
    let mut biggest = vec[0];
    let mut smallest = vec[0];
    for num in vec {
        if num > biggest {
            biggest = num;
        } 
        if num < smallest {
            smallest = num;
        }
    }

    biggest - smallest
}

fn calculate_difference_divides(vec: Vec<i32>) -> i32 {
    let mut biggest = 1;
    let mut smallest = 1;
    for i in 0..vec.len() {
        for j in 0..vec.len() {
            if i != j {
                if vec[i] % vec[j] == 0 {
                    biggest = vec[i];
                    smallest = vec[j];
                    break;
                }
            }
        }
    }

    biggest / smallest
}

#[test]
fn calculate_divides_works() {
    //For example, given the following spreadsheet:
    
    //5 9 2 8
    //9 4 7 3
    //3 8 6 5
    //In the first row, the only two numbers that evenly divide are 8 and 2; the result of 
    //this division is 4.
    //In the second row, the two numbers are 9 and 3; the result is 3.
    //In the third row, the result is 2.
    assert!(calculate_difference_divides(vec![5, 9, 2, 8]) == 4);
    assert!(calculate_difference_divides(vec![9, 4, 7, 3]) == 3);
    assert!(calculate_difference_divides(vec![3, 8, 6, 5]) == 2);
}

#[test]
fn calculate_checksum_part2_works() {
    //In this example, the sum of the results would be 4 + 3 + 2 = 9.
    let spreadsheet = vec![
        vec![5, 9 ,2, 8], // 4
        vec![9, 4, 7, 3], // 3
        vec![3, 7, 6, 5]  // 2
    ];

    assert!(calculate_checksum_part2(spreadsheet) == 9);
}

#[test]
fn calculate_difference_biggest_smallest_works() {
    //For example, given the following spreadsheet:

    //5 1 9 5
    //7 5 3
    //2 4 6 8
    //The first row's largest and smallest values are 9 and 1, and their difference is 8.
    //The second row's largest and smallest values are 7 and 3, and their difference is 4.
    //The third row's difference is 6.
    assert!(calculate_difference_biggest_smallest(vec![5, 1, 9, 5]) == 8);
    assert!(calculate_difference_biggest_smallest(vec![7, 5, 3]) == 4);
    assert!(calculate_difference_biggest_smallest(vec![2, 4, 6, 8]) == 6);
}

#[test]
fn calculate_checksum_works() {
    //In this example, the spreadsheet's checksum would be 8 + 4 + 6 = 18.
    let spreadsheet = vec![
        vec![5, 1, 9, 5], // 8
        vec![7, 5, 3],    // 4
        vec![2, 4, 6, 8]  // 6
    ];

    assert!(calculate_checksum(spreadsheet) == 18);
}
