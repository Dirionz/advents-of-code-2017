fn main() {
    let start = 277678;
    let b = (start as f64).sqrt() as i32 + 1;
    let n = get_next_prime(b);
    let matrix = create_spiral(n, n);
    let number = get_value_after_input_sum_of_neightbors(n, n, start);


    //print_matrix(matrix.clone());

    println!("{}", manhattan_distance(find_pos(matrix.clone(), start), find_pos(matrix.clone(), 1))); //475
    println!("{}", number); // 279138

}

fn print_matrix(matrix: Vec<Vec<i32>>) {
    for i in 0..matrix.len() {
        for j in &matrix[i] {
            if j < &10 {
                print!("{}\t\t\t", j);
            } else {
                print!("{}\t\t", j);
            }
        }
        println!("");
    }
}

fn get_next_prime(n: i32) -> i32 {
    let mut num = n;
    loop {
        let candidate = num;
        if is_prime(candidate as u64) == true { 
            return candidate;
        } else {
            num+=1;
        }
    }
}

fn is_prime(n: u64) -> bool {
    if n < 4 {
        // Skip all the following checks, we know it's a prime!
        return true;
    }

    // We don't need to check if n is 2 or 3 anymore,
    // since if n is one of these values, we won't reach here.
    if n % 2 == 0 {
        return false;
    }

    if n % 3 == 0 {
        return false;
    }

    let mut m = 5;
    while m <= (n as f64).sqrt() as u64 {
        if n % m == 0 {
            return false;
        }

        if n % (m + 2) == 0 {
            return false;
        }

        m += 6;
    }

    // Implicit return
    true
}

fn create_spiral(width: i32, height: i32) -> Vec<Vec<i32>> {
    let mut matrix = vec![vec![0; width as usize]; height as usize];

    let mut x: i32 = width / 2;
    let mut y: i32 = height / 2;
    
    let north = (0, -1);
    let south = (0, 1);
    let west =  (-1, 0);
    let east =  (1, 0);
    let mut direction = south;

    let mut dx = direction.0;
    let mut dy = direction.1;

    let mut c = 0;

    loop {
        
        c+=1;
        matrix[y as usize][x as usize] = c;

        let mut tmpdirection = direction;
        if direction == north {
            tmpdirection = west;
        } else if direction == south {
            tmpdirection = east;
        } else if direction == west {
            tmpdirection = south;
        } else if direction == east {
            tmpdirection = north;
        }

        let new_dx = tmpdirection.0;
        let new_dy = tmpdirection.1;

        let new_x = x + new_dx;
        let new_y = y + new_dy;

        if 0 <= new_x && new_x < width && 0 <= new_y && new_y < height 
            && matrix[new_y as usize][new_x as usize] == 0 {

            x = new_x;
            y = new_y;
            dx = new_dx;
            dy = new_dy;
            direction = tmpdirection;
        } else {
            x = x + dx;
            y = y + dy;
            if !(x <= x && x < width && 0 <= y && y < height) {
                return matrix;
            }
        }

    }

}

fn find_pos(matrix: Vec<Vec<i32>>, num: i32) -> (i32, i32) {
    for i in 0..matrix.len() {
        for j in 0..matrix[i].len() {
            if matrix[i][j] == num {
                return (j as i32, i as i32);
            }
        }
    }

    return (0, 0)
}

fn manhattan_distance(start_pos: (i32, i32), end_pos: (i32, i32)) -> i32 {
    ((start_pos.0 - end_pos.0).abs() + (start_pos.1 - end_pos.1).abs()) as i32
}


fn get_value_after_input_sum_of_neightbors(width: i32, height: i32, input: i32) -> i32 {
    let mut matrix = vec![vec![0; width as usize]; height as usize];

    let mut x: i32 = width / 2;
    let mut y: i32 = height / 2;
    
    let north = (0, -1);
    let south = (0, 1);
    let west =  (-1, 0);
    let east =  (1, 0);
    let mut direction = south;

    let mut dx = direction.0;
    let mut dy = direction.1;

    let mut c = 0;

    let neighbours: [(i32, i32); 8] = [(1, 0), (1, 1), (0, 1), (-1, 1), (-1, 0), (-1, -1),
                                           (0, -1), (1, -1)];

    loop {
        
        c=0;
        for i in 0..neighbours.len() {
            let n = neighbours[i];
            let new_x = x+n.0;
            let new_y = y+n.1;
            if new_x < width && new_y < height && new_x >= 0 && new_y >= 0  {
                c+=matrix[new_y as usize][new_x as usize];
            }
        }

        if c == 0 {
            c = 1;
        }
        if (c > input) {
            return c;
        }
        matrix[y as usize][x as usize] = c;

        let mut tmpdirection = direction;
        if direction == north {
            tmpdirection = west;
        } else if direction == south {
            tmpdirection = east;
        } else if direction == west {
            tmpdirection = south;
        } else if direction == east {
            tmpdirection = north;
        }

        let new_dx = tmpdirection.0;
        let new_dy = tmpdirection.1;

        let new_x = x + new_dx;
        let new_y = y + new_dy;

        if 0 <= new_x && new_x < width && 0 <= new_y && new_y < height 
            && matrix[new_y as usize][new_x as usize] == 0 {

            x = new_x;
            y = new_y;
            dx = new_dx;
            dy = new_dy;
            direction = tmpdirection;
        } else {
            x = x + dx;
            y = y + dy;
            if !(x <= x && x < width && 0 <= y && y < height) {
                return -1;
            }
        }

    }

}

#[test]
fn find_pos_works() {
    let b = (25 as f64).sqrt() as i32;
    let n = get_next_prime(b);
    let matrix = create_spiral(n, n);

    assert!(find_pos(matrix.clone(), 16) == (1, 0));
    assert!(find_pos(matrix.clone(), 13) == (4, 0));
    assert!(find_pos(matrix.clone(), 8) == (2, 3));
}

#[test]
fn manhattan_distance_works() {
    //Data from square 1 is carried 0 steps, since it's at the access port.
    //Data from square 12 is carried 3 steps, such as: down, left, left.
    //Data from square 23 is carried only 2 steps: up twice.
    //Data from square 1024 must be carried 31 steps.

    let b = (1024 as f64).sqrt() as i32;
    let n = get_next_prime(b);
    let matrix = create_spiral(n, n);

    assert!(manhattan_distance(find_pos(matrix.clone(), 1), find_pos(matrix.clone(), 1)) == 0);
    assert!(manhattan_distance(find_pos(matrix.clone(), 12), find_pos(matrix.clone(), 1)) == 3);
    assert!(manhattan_distance(find_pos(matrix.clone(), 23), find_pos(matrix.clone(), 1)) == 2);
    assert!(manhattan_distance(find_pos(matrix.clone(), 1024), find_pos(matrix.clone(), 1)) == 31);
}

#[test] 
fn get_value_after_input_sum_of_neightbors_works() {

    //147  142  133  122   59
    //304    5    4    2   57
    //330   10    1    1   54
    //351   11   23   25   26
    //362  747  806--->   ...
    let b = (24 as f64).sqrt() as i32;
    let n = get_next_prime(b);

    assert!(get_value_after_input_sum_of_neightbors(n, n, 57) == 59);
    assert!(get_value_after_input_sum_of_neightbors(n, n, 747) == 806);
    assert!(get_value_after_input_sum_of_neightbors(n, n, 10) == 11);
    assert!(get_value_after_input_sum_of_neightbors(n, n, 351) == 362);


}
